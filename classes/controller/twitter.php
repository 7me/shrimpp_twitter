<?php
/**
 * Fuel is a fast, lightweight, community driven PHP5 framework.
 *
 * @package    Fuel
 * @version    1.7
 * @author     Fuel Development Team
 * @license    MIT License
 * @copyright  2010 - 2013 Fuel Development Team
 * @link       http://fuelphp.com
 */

/**
 * The Welcome Controller.
 *
 * A basic controller example.  Has examples of how to set the
 * response body and status.
 *
 * @package  app
 * @extends  Controller
 */
namespace Twitter;

use Fuel\Core\Controller;
use Fuel\Core\Response;
use Fuel\Core\View;

class Controller_Twitter extends Controller
{
	public $test = "";
	
	/**
	 * The Skel balloon function
	 *
	 * @access  public
	 * @return  Response
	 */
	public function before()
	{
		parent::before();
		
		$fp = fopen("/var/tmp/test.txt", "a+");
		fwrite($fp, "Twitter::before");
		fclose($fp);
	}

	/**
	 * The Skel balloon function
	 *
	 * @access  public
	 * @return  Response
	 */
	public static function action_balloon()
	{	
	}

	public static function test()
	{
		$fp = fopen("/var/tmp/test.txt", "a+");
		//fwrite($fp, "Twitter::test");
		fclose($fp);
		return Model_Twitter::get_message();
	}
}
